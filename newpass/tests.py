from django.http import HttpRequest
from django.test import SimpleTestCase

from . import views

class GccTests(SimpleTestCase):
    def test_codigo_de_estado_OK(self):
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)

    def test_codigo_de_pagina_admin(self):
        response = self.client.get('admin/')
        self.assertEquals(response.status_code, 404)