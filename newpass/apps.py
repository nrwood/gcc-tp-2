from django.apps import AppConfig


class NewpassConfig(AppConfig):
    name = 'newpass'
