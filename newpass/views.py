from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Su contraseña se cambió correctamente')
            return redirect('home')
        else:
            messages.error(request, 'Corrija el error de abajo.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/newpass.html', {'form':form})

def crear_cuenta(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'Su cuenta se creo correctamente')
            return redirect('home')
        else:
            messages.error(request, 'Corrija el error de abajo.')
    else:
        form = UserCreationForm()
    return render(request, 'registration/crearCuenta.html', {'form':form})