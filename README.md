Trabájo práctico de Gestión de Centros de Cómputo - 2019

El trabajo consiste en realizar un programa de autenticación y cambio de contraseñas, en este caso con Python y Django, y desplegarlas en Heroku utilizando el CI/CD de gitlab.

Los "Stages" de la integración continua son:
 - Building: que comprueba que se instalan los requerimientos de la aplicacion. 
 - Testing: en el que se ejecutan las pruebas unitarias creadas.
 - Deploying: en el que se despliega la aplicacion en la plataforma de Heroku.

 Toda la configuración para esto se encuentra en el archivo ".gitlab-ci.yml" en el repositorio.